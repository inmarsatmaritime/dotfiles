# Inmarsat Maritime dotfiles

## Installation

```
$> cd ~
$> git clone https://christeraustad@bitbucket.org/inmarsatmaritime/dotfiles.git 
$> chmod +x ~/dotfiles/install.sh
$> ~/dotfiles/install.sh
```

This will clone the repo to your home directory under the forlder <code>dotfiles</code>. The **install.sh** script will install make sure you have **zsh** installed and install **oh-my-zsh**. The other configuration files in the **dotfiles** directory will be symlinked to the **~** directory as **.filname**.